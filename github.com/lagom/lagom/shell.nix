{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.sbt.override {
      jre = pkgs.jdk8;
    })
  ];
  JAVA_8_HOME = "${pkgs.jdk8}/lib/openjdk";
}
