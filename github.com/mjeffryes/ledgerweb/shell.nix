{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.ruby
    pkgs.rubyPackages.sinatra
    (pkgs.python3.withPackages (p: [
      #p.ledger
    ]))
  ];
}
