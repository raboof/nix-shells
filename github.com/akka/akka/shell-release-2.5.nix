{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
  name = "akka-2.5";

  targetPkgs = pkgs: [
    (pkgs.sbt.override {
      jre = pkgs.adoptopenjdk-hotspot-bin-11;
    })
    pkgs.graphviz
    #pkgs.diffoscope
    pkgs.multimarkdown
    pkgs.glibc
  ];

  extraBuildCommands = ''
    mkdir -p $out/usr/lib/jvm
    ln -s ${pkgs.openjdk8.home} $out/usr/lib/jvm/java-8-openjdk
    ln -s ${pkgs.adoptopenjdk-hotspot-bin-11.home} $out/usr/lib/jvm/java-11-adoptopenjdk
    '';
}).env
