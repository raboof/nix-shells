{ pkgs ? import <nixpkgs> {} }:

#let
#  nixpkgs-diffoscope = import (/home/aengelen/nixpkgs-diffoscope) { };
#in
pkgs.mkShell {
  name = "akka";
  buildInputs = [
    (pkgs.sbt.override {
      jre = pkgs.jdk11;
    })
    pkgs.graphviz
    pkgs.multimarkdown
    pkgs.diffoscope
    #nixpkgs-diffoscope.diffoscope
    #pkgs.jetbrains.idea-community
  ];

  JAVA_8_HOME = "${pkgs.jdk8}/lib/openjdk";
  JAVA_11_HOME = "${pkgs.jdk11}/lib/openjdk";

  #shellHook = ''sbt --java-home $JAVA_11_HOME -Dakka.genjavadoc.enabled=true "set every jdk8home:=\"$JAVA_8_HOME\"" shell'';
  #shellHook = ''sbt --java-home $JAVA_11_HOME -Dakka.genjavadoc.enabled=true "set every jdk8home:=\"$JAVA_8_HOME\"" shell'';
}
