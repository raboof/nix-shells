{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
  name = "akka-grpc";

  targetPkgs = pkgs: [
    (pkgs.sbt.override {
      jre = pkgs.jdk11;
    })
    #pkgs.diffoscope
    pkgs.multimarkdown
    pkgs.glibc
    pkgs.adoptopenjdk-hotspot-bin-8
  ];
  
  #runScript = "zsh";
}).env.overrideAttrs (old: {
  name = "akka-grpc";
})
