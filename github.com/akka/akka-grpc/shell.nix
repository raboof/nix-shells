{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "akka-grpc";

  buildInputs = [
    (pkgs.sbt.override {
      jre = pkgs.jdk11;
    })
    #pkgs.diffoscope
    pkgs.multimarkdown
    pkgs.glibc
    pkgs.adoptopenjdk-hotspot-bin-8
  ];
  
  #runScript = "zsh";
}
