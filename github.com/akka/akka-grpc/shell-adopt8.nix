{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
  name = "akka-grpc";

  targetPkgs = pkgs: [
    (pkgs.sbt.override {
      jre = pkgs.adoptopenjdk-hotspot-bin-8;
    })
    pkgs.glibc
    pkgs.adoptopenjdk-hotspot-bin-8
    pkgs.zsh
    pkgs.vim
    pkgs.git
  ];
  
  runScript = "zsh";
}).env
