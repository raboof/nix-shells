{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
  name = "akka-grpc";

  targetPkgs = pkgs: [ pkgs.sbt pkgs.glibc pkgs.jdk ];
  
#  runScript = "sbt";
}).env
