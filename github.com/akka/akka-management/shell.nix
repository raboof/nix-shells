{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  buildInputs = [
    (pkgs.sbt.override {
      # cluster-http/test fails on jdk16
      # due to reflection in mockito
      # https://github.com/akka/akka-management/issues/895
      jre = pkgs.jdk11;
    })
    # for scripts/authors.scala
    pkgs.scala
    pkgs.git
  ];
}
