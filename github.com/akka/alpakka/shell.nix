{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
  name = "alpakka";

  targetPkgs = pkgs: [
    #pkgs.sbt
    #pkgs.jdk
    pkgs.adoptopenjdk-hotspot-bin-8
    (pkgs.sbt.override {
      jre = pkgs.adoptopenjdk-hotspot-bin-8;
    })

    pkgs.which
    pkgs.ncurses5

    pkgs.glibc


    pkgs.docker-compose
    pkgs.git
    pkgs.vim
  ];
}).env
