{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  name = "akka-http";
  buildInputs = [
    (pkgs.sbt.override {
      #jre = pkgs.adoptopenjdk-hotspot-bin-8;
      jre = pkgs.jdk11;
    })
    #pkgs.git
    #pkgs.adoptopenjdk-hotspot-bin-8
    pkgs.graphviz
  ];

  shellHook = ''
    export SOURCE_DATE_EPOCH=`${pkgs.git}/bin/git log -1 --pretty=%ct`
  '';

}
