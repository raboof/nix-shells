{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.sbt.override {
      jre = pkgs.adoptopenjdk-hotspot-bin-11;
    })
    pkgs.git
    pkgs.adoptopenjdk-hotspot-bin-11
    pkgs.graphviz
  ];

  shellHook = ''
    export SOURCE_DATE_EPOCH=`${pkgs.git}/bin/git log -1 --pretty=%ct`
  '';

}
