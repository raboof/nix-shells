{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
  name = "play-grpc";

  targetPkgs = pkgs: [
    (pkgs.sbt.override {
      jre = pkgs.jdk11;
    })
    pkgs.glibc
    pkgs.jdk
  ];
}).env
