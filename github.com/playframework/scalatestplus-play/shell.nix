{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.sbt
    pkgs.chromium
    pkgs.chromedriver
  ];
}
