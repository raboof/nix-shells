{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.sbt.override {
      # expected in the scripted tests
      jre = pkgs.jdk8;
    })
  ];
}
