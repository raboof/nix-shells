{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.jq
    pkgs.python3Packages.virtualenvwrapper
    (pkgs.python3.withPackages (pp: [
      pp.setuptools
    ]))
  ];
}
