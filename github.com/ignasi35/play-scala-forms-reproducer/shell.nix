{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.sbt.override {
      jre = pkgs.jdk11;
    })
  ];
}
