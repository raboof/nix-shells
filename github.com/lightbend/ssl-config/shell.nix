{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.sbt.override {
      jre = pkgs.jdk8;
    })
    pkgs.jdk8
  ];
}
