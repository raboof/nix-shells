{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    # overriding the jre with jdk8 doesn't work for the SignatureSpec
    # overriding the jre with jdk11 works from 2.11.12 onwards
    (pkgs.sbt.override { jre = pkgs.jdk11; })
    pkgs.jdk
    pkgs.vim
    pkgs.which
    pkgs.gnupg
  ];
}
