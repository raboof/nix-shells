{ pkgs ? import <nixpkgs> { config.android_sdk.accept_license = true; } }:

pkgs.androidenv.buildApp { 
  name = "Alarmio";
  platformVersions = [ "28" ];
}
