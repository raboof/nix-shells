{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (p: [
      p.flask
      p.dateutil
      p.sh
      p.pystache
    ]))
    pkgs.ledger
  ];
}
