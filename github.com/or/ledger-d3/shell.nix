{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    (pkgs.python3.withPackages (p: [
      p.tornado
    ]))
    pkgs.ledger
  ];
}
