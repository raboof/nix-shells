# nix-shells

My `shell.nix` files to build various projects with [Nix](https://nixos.org/).

These can be conveniently loaded with [nix-shell-git](https://codeberg.org/raboof/nix-shell-git).
